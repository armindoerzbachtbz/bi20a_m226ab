## Dynamisches Binden Anhand von Beispiel in Wikipedia

Das folgende Beispiel wird im [Artikel von Wikipedia](https://de.wikipedia.org/wiki/Dynamische_Bindung) gezeigt:

```java
import java.util.*;

class Saeugetier {
    void steckbrief() {
        System.out.println("Ich bin ein Säugetier.");
    }
}

class Gepard extends Saeugetier {
    void steckbrief() {
        System.out.println("Ich bin ein Gepard.");
    }
}

class Elefant extends Saeugetier {
    void steckbrief() {
        System.out.println("Ich bin ein Elefant.");
    }
}

public class DynamischeBindungBeispiel {
    public static void main(String[] args) {
        List<Saeugetier> tiere = new ArrayList<Saeugetier>();
        tiere.add(new Saeugetier());
        tiere.add(new Gepard());
        tiere.add(new Elefant());

        for (Saeugetier tier: tiere) {
            tier.steckbrief();
        }
    }
}
```

Das dazugehörige Klassendiagramm sieht so aus:

![Klassendiagramm](./diagramm_wikipedia.drawio.png)

In der Hauptklasse `DynamischeBindungBeispiel`  werden 3 Objekte von verschiedenen Klassen erzeugt und in einer `ArrayList<Saeugetier>` mit dem Namen `tiere` versorgt. 

Der Typ der möglichen Elemente in `tiere` wird mit `<Sauegetier>` auf Subklassen der Klasse `Sauegetier` und die Klasse `Sauegetier` selbst beschränkt.

Da die Klasse `Saeugetier` eine Methode `steckbrief()` hat, kann man diese für alle Objekte in `tiere` sicher aufrufen. Das wird im `for`-Loop gemacht.

Die Frage ist aber, welche Methode dann für die Subklassen `Elefant` und `Gepard` aufgerufen werden? 

Der Output sieht wie folgt aus:
```
Ich bin ein Säugetier.
Ich bin ein Gepard.
Ich bin ein Elefant.
```

D.h. es wird `steckbrief()` von der Klasse `Gepard` bzw. `Elefant` aufgerufen, obwohl `tiere` mit dem Typ `Saeugetier` definiert wurde.

 Das aufrufen von `steckbrief()` geschieht **dynamisch** anhand des Objekt-Typs der im Array gespeichert ist und nicht anhand von der Deklaration der Variable `tiere`. Das ermöglicht **Polymorphismus** (griechisch für Vielgestaltigkeit).
