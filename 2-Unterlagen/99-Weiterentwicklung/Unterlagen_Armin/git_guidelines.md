# Warum sollten Sie bessere Commit-Messages schreiben?

Ich fordere Sie auf, ein persönliches Projekt oder ein beliebiges Repository zu öffnen und git log zu starten, um eine Liste alter Commit-Nachrichten anzusehen. Sie werden verstellen, dass sie ihre Commit-Messages von vor 6 Monaten höchstwahrschienlich nicht mehr verstehen.

Hier ein Beispiel von mir:

```
c7f9c4e (HEAD -> main, origin/main) Change the installation of fail2ban config files from ln to cp
bd24061 Adding filter for flavicon and Adding generation of website
6579eac add utility scripts
504d16c better skripts
7419169 sns
...
...
1562b17 perm
c591e46 perm
22136ea umleiten
6cfdf3a script
29eb013 scripts
02eea9e typo
2becf06 readme und cloundinit
```

Die meisten dieser Commit-Messages sind so grotten schlecht aus diesen Gründen:
- nur ein Stichwort
- null info warum der commit noetig war
- Unklar was genau gemacht wurde, oder wie soll ich wissen was ich mit `perm` gemeint habe?


# 5 Schritte zum Schreiben besserer Commit-Messages

Lassen Sie uns die vorgeschlagenen Richtlinien zusammenfassen:

- Grossschreibung und Interpunktion: Schreiben Sie das erste Wort gross und enden Sie nicht mit einem Satzzeichen. Wenn Sie konventionelle Commits (siehe unten) verwenden, denken Sie daran, alles klein zu schreiben.
- Grund für Commit angeben und benutzen sie Befehlsform(Imperativ): Verwenden Sie einen Grund für den Commit in der Betreffzeile. Beispiel -  `Add fix for dark mode toggle state`. Der Imperativ vermittelt den Eindruck, dass Sie einen Befehl oder eine Bitte aussprechen.
- Art der Übergabe: Geben Sie die Art der Übergabe an. Es ist empfehlenswert und kann sogar noch vorteilhafter sein, eine konsistente Reihe von Wörtern zu haben, um Ihre Änderungen zu beschreiben. Beispiel: Bugfix, Update, Refactor, Bump und so weiter. Weitere Informationen finden Sie im Abschnitt über konventionelle Commits weiter unten.
- Länge: Die erste Zeile sollte idealerweise nicht länger als 50 Zeichen sein.
- Inhalt: Seien Sie direkt und versuchen Sie, Füllwörter und Phrasen in diesen Sätzen zu vermeiden (Beispiele: obwohl, vielleicht, ich glaube, irgendwie).

# Konventionelle Commits

Vielleicht hate ihre Firma schon einen Vorlage für Commit-Messages. Ansonsten können sie sich an diese Halten:

```
<type>[optional scope]: <description>

[optional body]

[optional footer(s)]
```

Wobei sie für `<type>` eine Liste von verschiedenen fixen Commit-Typen benutzen wie z.B:

- feature - mit den Änderungen wird eine neue Funktion eingeführt
- fix - es wurde ein Fehler behoben
- refactor - umgestalteter Code, der **weder** einen Fehler behebt noch eine Funktion hinzufügt
- docs - Aktualisierungen der Dokumentation, z. B. der README oder anderer Markdown-Dateien
- style - Änderungen, die sich nicht auf die Bedeutung des Codes auswirken und wahrscheinlich mit der Formatierung des Codes zu tun haben, wie z.B. Leerzeichen, fehlende Semikolons, usw.
- test - Einfügen neuer oder Korrigieren früherer Tests
- perf - Leistungsverbesserungen
- ci - im Zusammenhang mit kontinuierlicher Integration
- build - Änderungen, die das Build-System oder externe Abhängigkeiten betreffen..


# Beispiel einer guten Commit-Message

Im Beipiel oben könnte man die Commit-Message `sns` ersetzen durch:
```
feature: Adding notification using AWS-SNS-Service to enable sending Emails
Refactoring Notification to be able to add other Notification means simply
Adding SNS Notification as one additional Notification Service.
```

Dann wüsste ich auch noch in 10 Jahren, was genau ich da dazugefügt habe. Aber eben. Die Zeit war knapp und ich habe mir die Arbeit erspart, was ich in 10 Jahren ganz sicher bereuen werde.

