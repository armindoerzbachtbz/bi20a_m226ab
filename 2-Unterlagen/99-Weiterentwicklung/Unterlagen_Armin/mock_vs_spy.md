# Mock vs. Spy

Mocking vs Spy.


## Think: Resourcen Lesen
- [https://www.baeldung.com/mockito-spy](https://www.baeldung.com/mockito-spy)
- [https://javapointers.com/java/unit-test/difference-between-spy-and-mock-in-mockito/](https://javapointers.com/java/unit-test/difference-between-spy-and-mock-in-mockito/)

## Pair: Zu zweit Unterschiede und Gemeinsamkeiten aufschreiben

Diskutiert Gemeinsamkeiten und Unterschiede zu zweit. Fasst diese in einem Markdown-, Text-, oder Word-Dokument zusammen.

## Share: Zusammentragen

### Gemeinsamkeiten von Spy und Mock
- man kann bei beiden Return-Werte für Methodenaufrufe definieren (when(...).thenReturn())
- man kann bei beiden mit verify() überprüfen ob Methoden aufgerufen werden, wie oft sie aufgerufen werden und in welcher Reihenfolge sie aufgerufen wurden [https://www.baeldung.com/mockito-verify](https://www.baeldung.com/mockito-verify)

### Unterschiede von Spy und Mock

| Spy           | Mock               |
|---------------|-----|
| Es wird ein reales Objekt der gemockten Klasse erzeugt | Es wird kein reales Objekt der Klasse erzeugt |
| Bei einem Methodenaufruf wird immer die Methode des realen Objektes aufgerufen, falls diese existiert | Es wird einfach ein Return-Wert zurück gegen |
| wenn kein Return-Wert definiert ist, und keine reale Methode existiert gibt es einen Fehler | wenn kein Return-Wert definiert ist, wird einfach ein Default-Wert für den Datentyp (null, 0 oder false) zurückgegeben |
| Ein Spy ist ein Wrapper um das reale Objekt | Ein Mock ist ein Objekt-Ersatz für ein reales Objekt |
| Man muss ein reales Objekt explizit Instanzieren. Das wird automatisch gemacht, wenn ein Konstruktor ohne Parameter existiert.<br>Sonst muss man ein Objekt explizit mit<br>`@Spy`<br>`Car testcar=new Car(100);` <br>erzeugen.| Man muss kein Objekt instanzieren |
| Damit kann ein Objekt *partiell* gemockt werden, d.h. ein Teil der Methoden werden real benutzt und andere Methoden werden durch `when().thenReturn()` definiert| Es werden alle Methoden gemockt |

