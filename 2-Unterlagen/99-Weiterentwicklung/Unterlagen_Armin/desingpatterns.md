
[TOC]
# Singleton

Es ist ein `creational` Design-Pattern. 

## Links zu Erklärungen

[https://javabeginnerstutorial.com/design-pattern/singleton-design-pattern-java/](https://javabeginnerstutorial.com/design-pattern/singleton-design-pattern-java/)  
[https://refactoring.guru/design-patterns/singleton](https://refactoring.guru/design-patterns/singleton)

## Was löst es?

- Sicherstellen das es maximal ein Instanz der Klasse gibt z.B eine Datenbankverbindung....

## Was sind die üblichen Probleme bei welchen Varianten?

- Einschränkung des Zugangs zur Instanz.
- Performance Probleme beim Zugriff auf Instanz.
- 2 Threads auf gleiche Instanz zugreifen, Dead-Locks.

## Welche Ansätze gibt?

- Es gibt eine `static` getInstance Methode, welche das Objekt zurückliefert und dieses beim ersten Aufruf erzeugt oder ein vorerzeugtes privates statisches Attribute der Singleton-Klasse selbst.
- Doppelter Check ob Instanz schon existiert, dann erst eine neue machen. Ein Check ausserhalb von synchronized block und einer innerhalb. (Lazy instanciation)
- Instanzierung während des Startups als Attribute in Singleton-Klasse, welches gleich mit `private static Klassenname singletonobject=new Klassenname()` erzeugt wird.
 
## Link zu Implementationen

[Java Logger Object](https://docs.oracle.com/javase/8/docs/api/java/util/logging/Logger.html#getLogger-java.lang.String-) ist ein Bespiel. Man kann zwar mehrer Logger-Objekte erzeugen, allerdings haben die dann alle verschiedene Namen.

[Java Runtime Objekt](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/lang/Runtime.html#getRuntime())

...


# Delegation- oder Proxy Pattern

Beim Proxy-Pattern handelt es sich um ein `structural` Design-Pattern.

## Links zu Erklärungen
[https://java-design-patterns.com/patterns/delegation/](https://java-design-patterns.com/patterns/delegation/)  
[https://refactoring.guru/design-patterns/proxy](https://refactoring.guru/design-patterns/proxy)


## Was löst es?

- Mehrere Vererbung haben moechte aber das nicht direkt implentieren kann.
- Es entkoppelt die Klassen.
- Es können mehrere Interfaces (Services) von einem "Controller" Implementiert werden. 
- Der Access zu einem Objekt kann eingeschränkt werden, indem Access-Rights geprüft werden.
- Es können Resultate gecached werden im Proxy.


## Was sind die üblichen Probleme bei welchen Varianten?
- Sie sind soweit wie moeglich entkoppelt.

## Welche Ansätze gibt?

- Es wird ein Interface (ServiceInterface) definiert. 
- Dieses enthält mit den möglichen *Operationen*. 
- Es gibt eine Proxy oder Controller Klasse welche dieses ServiceInterface implementiert
- Die Proxy-Klasse leitet für jede *Operation* die eigentlich Arbeit an die Implementationsklasse (Service) weiter
-  Dazu hat die Proxy-Klasse ein Attribute, in welchem es die Instanz der Implementationsklasse speichert.
-  Zusätzlich können vor dem Weiterleiten noch Checks (Berechtigungen, Valitität der Parameter,...) eingebaut werden.

## Links zu Implementationen
[Caching Service Results using Proxy-Pattern](http://www.javabyexamples.com/task-introduce-caching/)

# Observer-Pattern

Beim Observer-Pattern handelt es sich on ein `behavioral` Pattern
## Links zu Erklärungen
[https://java-design-patterns.com/patterns/observer/](https://java-design-patterns.com/patterns/observer/)  
[https://refactoring.guru/design-patterns/observer](https://refactoring.guru/design-patterns/observer)



## Was löst es?
- Es lösst das Problem, wie man eine Liste von Objekten, welche **Observer** genannt werden, mitteilen kann, wenn sich etwas am Zustand eines Objekts, welches **Subject** genannt wird, ändert.
- Es wird eine Meldung vom einem **Subject** an viele **Observer** gesendet, damit diese sich selbst wieder uptodate machen können.

## Was sind die üblichen Probleme bei welchen Varianten?
- Zufällige Reihenfolge welcher **Observer** benachrichtigt wird.

## Welche Ansätze gibt?
Publisher(Manager)-Klasse und Subscriber(Listener)-Interface Ansatz. Das Publisher Objekt entspricht dem **Subject** und für selbst eine Liste mit Subscribern. Die Subsriber sind die **Observer**. Ein Subscriber muss eine Methode `update` oder so imlementieren, welche vom Publisher aufgerufen werden kann. Der Publisher hat 3 Methoden:
    - notifySubsribers() : Trigger einen for-Loop ueber die Liste der Subscriber
    - subscribe(Subcriber): Fuegt den Subscriber zur Liste der Subscriber dazu
    - unsubscribe(Subscriber): Entfernd den Subscriber von der Liste der Subscriber
Jede Klasse die das Subsriber Interface implementiert kann sich beim Publisher registrieren.


## Links zu Implementationen
 
[ChangeListerer](https://docs.oracle.com/javase/8/docs/api/javax/swing/event/ChangeListener.html)
[PropertyChangeListerer](https://docs.oracle.com/javase/tutorial/uiswing/events/propertychangelistener.html)

# Factory-Method

Beim Factory-Method-Pattern handelt es sich on ein `creational` Pattern
## Links zu Erklärungen
[https://java-design-patterns.com/patterns/factory-method/](https://java-design-patterns.com/patterns/factory-method/)  
[https://refactoring.guru/design-patterns/factory-method](https://refactoring.guru/design-patterns/factory-method)



## Was löst es?
- Es ermöglicht die Erzeugung von Objekten verschiedenen Klassen über dieselbe Methode.
- Es "versteckt" das eigentliche erzeugen vom Objekt hinter einem Interface. 
- Es wird eine erzeuger Objekt oder einer Erzeuger-Methode für eine ganze Gruppe von Klassen erzeugt.
- Dadurch kann einen Gruppe von Objekten durch eine andere ersetzt werden indem man eine andere Erzeugerklasse instanziert.


## Was sind die üblichen Probleme bei welchen Varianten?
- Viele Subklassen nötig

## Welche Ansätze gibt?
- Es gibt eine Abstrakte Klasse, bzw ein Interface das für jeden Klassen-Typ eine createKasse() Methode definiert.
- Es gibt für jede Gruppe von Klassen eine Klassen-Typ spezifische Implemntation dieser Factory-Klasse, welche alle createKlasse()-Methoden des Interfaces implementiert. D.h. zum Beispiel für alle MySQL-Datenbank-Objekte eine Klasse und fuer alle MongoDB-Datenbank-Objekte eine andere.
- Dadurch kann einfach durch das Instanzieren der richtigen Factory-Klasse einen Switch zwischen Mongo-DB und Mysql-DB gemacht werden.
- Für alle zu erzeugenden Objekte braucht es auch noch ein Interface.

## Links zu Implementationen
 
[Charset.forname()](https://docs.oracle.com/javase/8/docs/api/java/nio/charset/Charset.html#forName-java.lang.String-)

