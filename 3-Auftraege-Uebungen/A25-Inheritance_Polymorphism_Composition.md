# Inheritance Polymorphism

**Inhalt**

[TOC]

<br>Zeitbedarf: 120-240 min

Nach einer kurzen Instruktion arbeiten Sie selbständig an diesem Auftrag. Sie können sich helfen lassen, aber Sie müssen die Aufgabe selber lösen und auch auf Ihrem Rechner zeigen können.

Sie werden dazu die vorgefertigten Code-Teile brauchen, den Sie in der Datei [Social-Network.zip](../2-Unterlagen/04-Dynamische-Bindung-Polymorphie/Inheritance_Polymorphism_Composition/SocialNetwork.zip) finden.

Aus didaktischen Gründen werden Sie zuerst zu einem falschen Ansatz verleitet. Lassen Sie das zu und machen Sie alles im Detail durch und zeigen Sie der Lehrperson die Resultate und die Zwischenresultate. Erstellen sie dazu ein **Projekt in IntelliJ** mit **Git-Repo** und bei jedem Zwischenschritt machen sie einen seperaten **Commit**

Wenn Sie fertig sind, geben Sie die Aufgabe ab. Laden sie dazu den Lehrer ein (armindoerzbachtbz)

Zusätzlich zeigen Sie die Arbeit der LP.

# Start

**Vererbung & Polymorphismus**

Was wir lernen werden:

In dieser Sitzung werden wir uns mit dem OO-Konzept "Vererbung" beschäftigen.

Wir werden lernen, was Polymorphismus bedeutet.

Wir werden auch die Konzepte des Überladens und Überschreibens kennenlernen.

# Social-Network

Bei dieser Aufgabe geht es darum, ein bestehendes Programm Social-Network (Quellcode:[Social-Network.zip](../2-Unterlagen/04-Dynamische-Bindung-Polymorphie/Inheritance_Polymorphism_Composition/SocialNetwork.zip)) zu erweitern und zu verbessern.

## Erweitern von Social-Network, falscher Ansetzt

Um die Vorteile der Vererbung zu verstehen, werden wir einen Simulator für soziale Netzwerke programmieren. 

Zuerst programmieren Sie ihn auf die falsche Weise. 

Wir werden feststellen, wie
dieser Ansatz Wartung und Erweiterungen schwieriger und komplexer macht.

Wir wollen ein Programm mit folgenden Klassen:

- **MessagePost:** für Nachrichten.
- **PhotoPost:** für Fotos.
- **NewsFeed:** enthält eine Sammlung von Nachrichten und Fotos.
- **Starter:** Das Hauptprogramm

![Class-Diagramm](./A25_1_ohne.drawio.png)
Installieren Sie die Klassen aus dem heruntergeladenen QuellCode [SocialNetwork.zip](../2-Unterlagen/04-Dynamische-Bindung-Polymorphie/Inheritance_Polymorphism_Composition/SocialNetwork.zip).

Jede Post-Klasse hat eine display-Methode, um Details des Posts zu drucken.

### Übung – Event Post dazufügen

Erweitern Sie das SocialNetwork um eine neue Art von Post-Klasse:

**EventPost:** für Ereignisse.

Die Klasse hat folgende Attribute:

```java
private String author;
private long timeStamp;
private int pages;
private int likes;
private ArrayList<String> comments;
```
Implementieren Sie den Konstruktor und die erforderlichen Methoden entsprechend.

### Übung - Erweitern der NewsFeed-Klasse mit EventPost

Nun müssen wir natürlich diesen neuen Post-Typ zu unserer *NewsFeed* Klasse hinzufügen.
```java
private ArrayList<EventPost> events;
```

Außerdem müssen wir diese Liste auch im Konstruktor initialisieren. Und fügen die
notwendigen Methoden.

**Analyse:**

Dieser Ansatz ist natürlich sehr mühsam und fehleranfällig.

Nennen Sie die Hauptprobleme bei diesem Ansatz:

|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |

## Analyse der falschen Lösung

Ein Hauptproblem ist der doppelte Code, den wir hinzufügen. Die meisten Attribute in allen
drei Post-Klassen sind die gleichen.

Und wir müssen die NewsFeed-Klasse erweitern, mit Code, der sich wiederholt und
umständlich ist. Bei der Erweiterung des Codes kann es leicht zu Fehlern kommen. Wir könnten sogar
bestehenden Code zerstören.

Und es gibt noch mehr: Wenn wir uns entscheiden, das Kommentarattribut von
`ArrayList<String>` in `ArrayList<Comment>` zu ändern, müssen wir dies an mehreren
Punkten des Codes ändern.

Eine Idee: Wenn wir unser Programm erweitern, wollen wir nur die neuen Klassen hinzufügen. Aber wir
wollen die übrigen Klassen nicht ändern. Wenn wir zum Beispiel eine neue Post
Klasse hinzufügen, wollen wir die NewsFeed-Klasse nicht ändern.

## Schritt für Schritt zu besserem Coding

Vererbung ist ein wichtiges Konzept in der objektorientierten Programmierung. Klassen
können von anderen Klassen erben. Das heißt, eine Klasse kann Attribute und
Methoden von einer anderen Klasse erben.

In einem ersten Schritt wollen wir eine `Post`-Klasse implementieren, die alle gemeinsamen
Attribute der verschiedenen Post-Klassen vereint. Von dieser Superklasse werden alle Post-Klassen
Attribute und Methoden erben. Falls nötig, kann eine Post-Klasse ihre eigenen
spezielle Attribute haben.

So ist die Vererbung in Java programmiert:

### Übung - Erstellen einer neuen Post-Klasse als Superklasse

Identifizieren Sie die gemeinsamen Attribute der Post-Klassen und fügen Sie diese zu einer neuen Post
Klasse hinzu. Diese Klasse ist die Superklasse aller anderen Post-Klassen.

Die neue Struktur sollte wie folgt aussehen:
![](./A25_1_mit.drawio.png)
### Übung - Vereinfachung der NewsFeed-Klasse

Refaktorieren Sie die Klasse `NewsFeed` entsprechend. Wir wollen eine `ArrayList`, die alle
mit allen Beiträgen.

## Finales Produkt

Die Klasse `NewsFeed` arbeitet nur mit der neuen Klasse `Post` und hat keine
keine Kenntnis von den Unterklassen.

Dies wird spätere Erweiterungen vereinfachen.

![](./A25_1_mit.png)

### Einfacher Ausdruck und ein intelligenterer Ausdruck von Attributen

Stellen Sie sicher, dass Ihre Superklasse `Post` über die Methode `display()` verfügt.

1.  Wir wollen eine Einfache Ausgabe der allgemeinen Attribute, die jeder Beitrag hat.

2. Jetzt geben sie auch die speziellen Attribute der einzelnen erweiterten Post-Unterklassen aus. Was müssen Sie in der Methode der Unterklasse tun, um die beiden Attribute (von Oberklasse und Unterklasse) auszugeben?

3. Zeigen Sie Ihre Ergebnisse der Lehrkraft.

## Informationen zum Überschreiben von Methoden
Beim Ausdruck von Attributen müssen Sie darauf achten, dass Ober- und Unterklassen
zusammenarbeiten. Insbesondere ist darauf zu achten, dass die display-Methode der
Oberklasse in der Unterklasse überschrieben wird. Einige Tipps dazu finden Sie auf
folgenden Webseiten:

<http://docs.oracle.com/javase/tutorial/java/IandI/override.html>

<https://beginnersbook.com/2014/01/method-overriding-in-java-with-example/>

| Notes |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|-------|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
|       |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|       |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|       |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|       |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|       |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|       |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|       |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|       |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |

