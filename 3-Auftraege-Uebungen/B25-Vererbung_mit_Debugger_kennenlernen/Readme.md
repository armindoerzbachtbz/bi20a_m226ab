# Vererbung mit Debugger kennenlernen

In diesem Auftrag geht es darum, dass ihr versteht, wie Vererbung wirklich funktioniert.
Wir werden Anhand vom Code vom [Auftrag A25](../A25-Inheritance_Polymorphism_Composition.md) versuchen zu verstehen wir Java (oder die meisten anderen Programmiersprachen auch) vererbung implementiert.

## Vorbereitung

1. Dazu ladet ihr folgendes [B25-Vererbung_mit_Debugger_kennenlernen.zip](./B25-Vererbung_mit_Debugger_kennenlernen.zip) herunter und extrahiert die Files.
2. Das so entstandene Versichenis B25-Vererbung_mit_Debugger_kennenlernen sollt ihr nun in IntellJ mit  File -> Open öffnen.
3. Öffnet das File `Starter.java`. ![Alt text](image-1.png)
4. Ihr werdet eine Meldung kriegen `Project JDK is not defined`. Klickt auf `Setup JDK` und wählt irgendeine Javaumgebung aus.![Alt text](image-2.png)
5. Jetzt könnt ihr das `Current File` mal laufen lassen.
   ![Alt text](image-3.png) 

Der output sollte ungefähr so aussehen:
```
..
.. Ganz viel output...
..
Username Eve
Posted 0 seconds ago

   1 comment(s):
    - bloeder post
Pictures filename is:eve.jpg
Caption is:Eve 2012

```

## Aufträge

1. Als erstes sollt ihr einen Breakpoint setzen. Damit markiert ihr eine Zeile vor welcher die Ausführung des Codes gestoppt wird wenn ihr den Code im Debug Modus startet. Dazu klickt ihr einfach auf die Zeilennummer wo ihr den Breakpoint setzen wollt. ![Breakpoint setzen](image.png)
2. Als nächstes startet ihr den Debugger. ![](image-4.png)
3. Er wird bei der Markierten Zeile stoppen. Dann sehen sie ungefähr folgendes: ![Alt text](image-5.png) 
   Sie sehen alle gesetzen Variabeln rechts und den Stacktrace links.
4. Jetzt haben sie folgende Möglichkeiten das Programm fortzusetzen:
![Alt text](image-6.png)
   Benutzen sie Step-Into um in der Zeile: 
   `NewsFeed news = new NewsFeed();` zu schauen was bei einen new NewsFeed() genau passiert. 
5. Wenn sie alles richtig gemacht haben, landen sie im Konstruktur der Klasse `NewsFeed`. Jetzt benutzen sie 3 mal Step-Over bis sie wieder in der `main`-Methode der `Starter`-Klasse landen.
6. Was geschieht wenn sie mit `Step-Into` die Zeile 7 abarbeiten? Benutzen sie immer Step-Into und schreiben sie sich auf durch, welche Methoden welcher Klassen durchlaufen werden, bis sie wieder in der `main`-Methode der `Starter`-Klasse landen.
7. Jetzt benutzen sie `Step-Over` bis zur Zeile 16. Dort benutzen sie `Step-Into` um zu schauen was genau in `news.show()` passiert. Schauen sie genau hin, welche Methoden von welchen Klassen sie durchlaufen, und was für Variablen wo gesetzt sind.
8. Was ist die Variable `this`?

Wenn sie nun die ganze Wahrheit erfahren wollen was passiert, können sie `Do not step into the classes`-Einstellungen unter `Settings` -> `Build,Execution, Deployment` -> `Debugger` -> `Stepping` abschalten:
![](image-7.png)

Dann können sie den Code nochmals mit `Step-Into` durchlaufen und können zuschauen, was alles noch gemacht wird, wenn Objekte erzeugt werden, oder zum Beispiel ein einfacher `String` mit `System.out.println()` ausgeben wird. Erschrecken sie nicht, wenn es ein bisschen komplizierter wird.