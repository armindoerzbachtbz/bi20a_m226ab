# Observer- und Factory-Method-Design-Pattern

In diesem kleinen Auftrag geht das darum folgende beiden Design-Pattern kennen zu lernen:
- [Observer Pattern](https://en.wikipedia.org/wiki/Observer_pattern)
- [Factory Method Pattern](https://refactoring.guru/design-patterns/factory-method)

Ihr sollt dies als Teamarbeit zu zweit machen.
Jeder erstellt für ein Design-Pattern eine kurze Zusammenfassung und erklärt danach anhand dieser das Design-Pattern seinem Team-Kollegen.

# Vorgehen
- Schaut euch die [ChatGPT Antworten(siehe unten)](./Readme.md#chatgpt-antworten) auf die Fragen "Was ist ...?" und beantwortet folgende Fragen:
  - Zu welchem Typ Design-Pattern gehört es? (Creational,Structural,Behavioral)
  - Welche allgemeine Problemstellung löst das Design-Pattern?
  - Wie wird das Problem gelöst?
- Versucht die beiden Patterns anhand vom obigen Links zu verstehen und beandwortet folgende Fragen:
  - Welche Klassen und Interfaces werden normalerweise eingesetzt?
  - Welche Methoden werden wo definiert und wo implementiert?
  - Welche Probleme gibt es bei der Implementation zu beachten?
  - Welche Vorteile hat dieses Design-Pattern?
  - Welche Nachteile hat dieses Design-Pattern?
- Sucht nach einem guten Anwendungsbeispiel und verlinkt dieses.
  




## Chatgpt-Antworten
### Antwort auf "Was ist das Observer-Design-Pattern?"
Das Observer-Design-Pattern ist ein entwurfsmuster (Design Pattern) aus dem Bereich der Softwareentwicklung, das zur Kategorie der Verhaltensmuster gehört. Es wird verwendet, um eine Abhängigkeit zwischen Objekten zu definieren, sodass Änderungen im Zustand eines Objekts an alle abhängigen Objekte automatisch weitergegeben werden.

Das Observer-Pattern besteht aus zwei Hauptkomponenten:

1. **Subject (Subjekt):** Das ist das Objekt, dessen Zustandsänderungen überwacht werden. Es hält eine Liste der abhängigen Objekte, die als "Observer" bezeichnet werden.

2. **Observer (Beobachter):** Dies sind die Objekte, die den Zustand des Subjects überwachen. Sie registrieren sich beim Subject, um über Änderungen informiert zu werden, und werden benachrichtigt, wenn der Zustand des Subjects aktualisiert wird.

Das Observer-Pattern ermöglicht eine lose Kopplung zwischen Subject und Observer. Das bedeutet, dass Änderungen im Subject oder in den Observers unabhängig voneinander vorgenommen werden können, ohne dass dies zu größeren Auswirkungen auf das System führt.

Ein einfaches Beispiel könnte ein Nachrichtendienst sein, bei dem das Subject die Nachrichtenquelle ist und die Observers die verschiedenen Abonnenten, die Benachrichtigungen über neue Nachrichten erhalten möchten.

In vielen Programmiersprachen, wie Java, gibt es bereits eingebaute Mechanismen für die Implementierung des Observer-Patterns. In Java wird beispielsweise das Interface `Observer` und die Klasse `Observable` verwendet. Es gibt jedoch auch andere Möglichkeiten, das Observer-Pattern zu implementieren, je nach den Anforderungen der Anwendung und der Programmiersprache.

## Antwort auf "Was ist das Factory-Method-Pattern?"
Das Factory-Methoden-Muster (Factory Method Pattern) ist ein Entwurfsmuster aus der Softwareentwicklung, das zur Erzeugung von Objekten dient. Es gehört zur Kategorie der sogenannten "Erzeugungsmuster" (Creational Patterns) und wird verwendet, um die Erzeugung von Objekten zu kapseln, indem eine Schnittstelle für die Erstellung von Objekten definiert wird, aber die konkrete Implementierung dieser Schnittstelle den abgeleiteten Klassen überlassen wird.

Das Muster besteht aus einer abstrakten Fabrikklasse, die eine Methode definiert, um ein Objekt zu erstellen (die sogenannte "Factory-Methode"). Die konkreten Unterklassen dieser abstrakten Fabrikklasse implementieren dann diese Factory-Methode, um spezifische Objekte zu erzeugen. Durch dieses Muster wird die Wahl der Klasse, die erstellt wird, auf die abgeleiteten Klassen verschoben, wodurch die Abhängigkeit von konkreten Klassen in anderen Teilen des Codes reduziert wird.

Hier ist ein einfaches Beispiel in Java:

```java
// Abstrakte Fabrikklasse
abstract class Creator {
    public abstract Product factoryMethod();
}

// Konkrete Fabrikklasse
class ConcreteCreatorA extends Creator {
    public Product factoryMethod() {
        return new ConcreteProductA();
    }
}

// Ein weiterer konkreter Fabrikklasse
class ConcreteCreatorB extends Creator {
    public Product factoryMethod() {
        return new ConcreteProductB();
    }
}

// Abstrakte Produktklasse
interface Product {
    void operation();
}

// Konkrete Produktklasse A
class ConcreteProductA implements Product {
    public void operation() {
        System.out.println("Operation in ConcreteProductA");
    }
}

// Konkrete Produktklasse B
class ConcreteProductB implements Product {
    public void operation() {
        System.out.println("Operation in ConcreteProductB");
    }
}
```

In diesem Beispiel definiert die abstrakte Klasse `Creator` die Factory-Methode `factoryMethod()`, die von den konkreten Fabrikklassen (`ConcreteCreatorA` und `ConcreteCreatorB`) implementiert wird. Jede konkrete Fabrikklasse erzeugt eine spezifische konkrete Produktklasse (`ConcreteProductA` bzw. `ConcreteProductB`). Der Client-Code erstellt dann eine Instanz der abstrakten Fabrikklasse und ruft die Factory-Methode auf, um ein konkretes Produkt zu erhalten, ohne die genaue Implementierung des Produkts zu kennen.