## Wissensaneignung 2 (OOP)

- [Buch Java 9 Kap.1-7, E. Fuchs, 2017, HERDT](../2-Unterlagen/00-Buecher/Buch__Java_9_Grundlagen_Programmierung/JAV9.pdf)

Zeitbedarf 2-6 Std (Teils als Hausaufgabe)

Lesen Sie Kapitel 1 bis Kapitel 7 (S. 88) aufmerksam durch und lösen Sie dann die Wissenstests 1 und 2.
Die beiden Wissenstests sind als PDF zu speichern (Ausdrucken als PDF) und vor dem Fälligkeitsdatum hier hochzuladen.
 - [JAVA 9 Grundlagen 1 - Einführung, Programmentwicklung und grundlegende Sprachelemente](https://shop.herdt.com/quiz/jav9/java9e28093grundlagen1/quiz/#/quiz)
 - [JAVA 9 Grundlagen 2 - Kontrollstrukturen, Klassen, Attribute, Methoden](https://shop.herdt.com/quiz/jav9/java9e28093grundlagen2/quiz/#/quiz) 
 
(Die Wissenstests sind nicht Notenrelevant)