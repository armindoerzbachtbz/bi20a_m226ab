# Grundsätze der OO-Programmierung

## Historische Entwicklung der Programmierung

** Einstiegsfrage**
Wieso entstehen immer neue Programmiersprachen?

Video zur Geschichte der Programmiersprachen:

- [https://www.youtube.com/watch?v=qLWb94Exc-o](https://www.youtube.com/watch?v=qLWb94Exc-o)<br>10 min

** Aufgabe dazu in Teams zu 2-4 Pers**
Objektorientierte Programmierung vs. Prozedurale Programmierung.

Wieso entstehen neue Konzepte?

Schauen Sie die 2 Videos dazu an zum Thema der Unterscheidung zwischen Objektorientierter 
und Prozeduraler Programmierung an und diskutieren Sie zusammen.

- https://www.youtube.com/watch?v=4DDSUYhJIFc <br>8 min
- https://www.youtube.com/watch?v=ese5Udwgwzc <br>5 min



## Grundsätze der Objektorientierten Programmierung

Zum Lesen: 
- https://www.freecodecamp.org/news/object-oriented-programming-concepts-21bb035f7260/ <br>15 min 

- https://beginnersbook.com/2013/04/oops-concepts/ <br>15 min nur bis und mit Kapitel 4, wobei das wichtigste in Kapitel 4 steht.

(siehe auch Kapitel 2 aus dem Buch Ruggerio, Compendio -> lesen.)


Beschreiben sie mit eigenen Worten 4 von (oder alle) 6 Konzepte 
der OO-Programmierung.


Abgabe als Markdown-Dokument in ihrem GIT-Repo.