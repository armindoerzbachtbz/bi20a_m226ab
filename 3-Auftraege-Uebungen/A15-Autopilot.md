# Aufgabe Autopilot

**Ausganslage**: Sie sollen ein Programm schreiben, das eine den folgenden Vorgang in Objektorientierter Sprache Abbildet:

Der Autopilot des Zugs kriegt das Signal zum Abfahren.

Er löst seine Bremsen des Zugs.

Er regelt den Strom des Motors hoch.


**Implemtationsdetails**:
Dabei sollen Autopilot, Bremsen und Motor als Klassen implementiert werden die Signale(Meldungen) empfangen können und untereinander in Beziehung stehen. Was sie mit den Signalen machen ist Ihnen überlassen. Sie koennen eine Simple Ausgabe machen, aber auch etwas komplexeres.

**Hinweis**: Wie kann der Autopilot auf das Objekt der Klasse Bremsen zugreifen? Was braucht es dazu in der Klasse Autopilot?

Abgabe: Alle Klassen als .java Files oder zusammengestellt in einem PDF (via Word)
