# Module M226a und M226b
[TOC]
# M226a - Klassenbasiert (ohne Vererbung) implementieren
[Modulidentifikation M226a](https://www.modulbaukasten.ch/module/226A/4/de-DE?title=Klassenbasiert-(ohne-Vererbung)-implementieren)

## Leistunsbeurteilungen

### M226a LB1 (15%, mündliche Einzelprüfung, 12 min)
Themen: UML, OO-Prinzipien


### M226a LB2 (30%, schriftliche Prüfung, 60 min)
Themen: UML, OO-Prinzipien, ...


### M226a LB3 (55%, praktisches Projekt) 
Bewertungskriterien:<br>
Es müssen alle Elemente im Buch M226 von Ruggerio, Compendio von Kap. 5 bis 11, sowie Kap. 14 und 15 enthalten sein.
- Wer das Minimum des Kap. 13 macht, kann maximal die Note 4.7 erreichen.
- Wer ein eigenes Projekt "gut" abschliesst inkl. "Doku", "JavaDoc" und "JUnit-Tests", kann eine Note 6 machen. |


## Bücher 

[Buch M226_Ruggerio_Compendio](./2-Unterlagen/00-Buecher/Buch__M226_Ruggerio_Compendio/)

## UML-Tools
- https://staruml.io/download
- https://umletino.com

## Fächertagebuch

| Tag  | Auftrag/Übung | Inhalte/Themen |
| ---- | ------------- | ------------------------ |
| 22.08.23 | [A11](./3-Auftraege-Uebungen/A11-Wissensaneignung1.md)            |  Modulvorstellung <br> Installation Eclipse oder ähnliche Programmierumgebung (Buch Kap. 12.1)<br> Beginn mit Buch/Skript Compendio 226 selbständig durchmachen (Teil A (Kap. 1-4))                     |
| 29.08.23 | [A12](./3-Auftraege-Uebungen/A12-WissensaneignungUML.md),[A13](./3-Auftraege-Uebungen/A12-GrundsaetzeDerOOProgrammierung.md)            |  Wissensaufbau zu Objektorientierter Programmierung und Unified Modeling Language (UML)                                       |
| 05.09.23 | [A13](./3-Auftraege-Uebungen/A12-GrundsaetzeDerOOProgrammierung.md)            |  Weiterarbeit am Wissensaufbau, <br>*Input* über UML-Zusammenhänge                                        |
| 12.09.23 |             |  **LB3** Definition eigenes Projektes (max 3 Pers)<br>(Projektumriss, Anforderungsdefinition, Activity-Diagram, Use-cases, ERM?, Class-Diagram, Sequence-Diagram)<br> **LB1** Ab dem 2. Teil des Halbtages laufend Kompetenzabnahmen/Basic-Check (mündlich einzeln, Teil A im Buch)  |
| 19.09.23 ||             |  **LB1** Basic-Check (Fortsetzung) <br>Beginn mit dem eigenen Projekt (Planung/Konzept, UML). Lassen Sie sich von Kap. 13 inspirieren. Bedingung: Es müssen alle Elemente von Kap. 5 bis 11, sowie 14 und 15 enthalten sein.<br><br>Der LP die Aufgabenstellung aufzeigen. Diagramm(e) & Prosa   |
| 26.09.23 |             |  **LB2** Schriftliche Prüfung, 30% ModulNote<br>Weiterarbeit am Projekt                     |
| 03.10.23 |             |  Arbeit am Projekt<br>Präsentierung Zwischenstand des Projektes (v.a. eine Herausforderung)                     |
| 24.10.23 |             |  Arbeit am Projekt<br>Projektbeobachtung durch LP |
| 31.10.23 |             |  Arbeit am Projekt<br>Projektbeobachtung durch LP / erste Projektabnahmen  |
| 07.11.23 |             |  **LB3** Projektabschluss, Projektdemos<br>Projektbesprechung/Notengebung |

# M226b - Objektorientiert (mit Vererbung) implementieren

[Modulidentifikation M226b](https://www.modulbaukasten.ch/module/226B/4/de-DE?title=Objektorientiert-(mit-Vererbung)-implementieren)

## Leistunsbeurteilungen

###  M226b LB1 (30%, schriftliche Prüfung, 60 min)
Themen: OO-Prinzipien, Vererbung, Polymorphismus, Design Pattern

###  M226b LB2 (30%, Qualität und Quantität der Übungen)
Themen: Vererbung, Polymorphismus, Abstrakte Klassen

###  M226b LB3 (40%, Pairprogramming-Miniprojekt)
Thema: Selbstdefinition - Bearbeitungszeit 15-20 Std. (teilweise in Hausarbeit)

## Bücher und Unterlagen
[Buch Java_9_Grundlagen_Programmierung](./2-Unterlagen/00-Buecher/Buch__Java_9_Grundlagen_Programmierung/JAV9.pdf)

[Dazugehörige Source Dateien (*.java) JAV9_Arbeitsdateien.zip](2-Unterlagen/00-Buecher/Buch__Java_9_Grundlagen_Programmierung/JAV9_Arbeitsdateien.zip)

## Fächertagebuch

| Tag  | Inhalte/Themen, Aufträge/Übungen |
| ---- | -------------------------------  |
| 14.11.23 | [Dynamisches Binden ](./2-Unterlagen/04-Dynamische-Bindung-Polymorphie/wikipedia.md) <br>[Polymorphismus A25 **Bewertete Übung LB2**](3-Auftraege-Uebungen/A25-Inheritance_Polymorphism_Composition.md) |
| 21.11.23 | [Abstrakte Klassen **Bewertete Übung LB2**](./2-Unterlagen/03-Vererbung/uebung_vererbung_Sensor/Readme_de.md) |
| 28.11.23 | [Design Patterns](/2-Unterlagen/99-Weiterentwicklung/Unterlagen_Armin/M226b_Design_Patterns.pdf) <br> [Singleton Pattern](https://javabeginnerstutorial.com/design-pattern/singleton-design-pattern-java/) <br> [Delegation Pattern](https://java-design-patterns.com/patterns/delegation/) |
| 05.12.23 | **Späteste Abgabe Übungen LB2** <br> [Mit Debugger Vererbung kennenlernen](./3-Auftraege-Uebungen/B25-Vererbung_mit_Debugger_kennenlernen/Readme.md) |
| 12.12.23 | Ecolm-Test **LB1** <br>[Auftrag Observer- und Factory-Method-Design-Pattern](./3-Auftraege-Uebungen/B26_observer_factory_method/Readme.md) <br> Start **LB3** |
| 19.12.23 | [Zusammenfassung Desing-Pattern](./2-Unterlagen/99-Weiterentwicklung/Unterlagen_Armin/desingpatterns.md)<br>[Junit-Tests und Mocking](https://gitlab.com/armindoerzbachtbz/passabene_kassenzettel_mocking) <br> Arbeit an LB3 |
| ---- | ---- Weihnachtsferien ----       |
| 09.01.24 | Arbeit an LB3          |
| 16.01.24 | [Mock vs. Spy](./2-Unterlagen/99-Weiterentwicklung/Unterlagen_Armin/mock_vs_spy.md)<br>Arbeit an LB3          |
| 23.01.24 | Arbeit an LB3          |
| 30.01.24 | Abgabe **LB3**: <br><li>in Gitrepo</li> <li>letzter Commit 08:00 30.1.24  </li>       |
